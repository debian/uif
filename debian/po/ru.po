# translation of ru.po to Russian
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
#
# Yuri Kozlov <kozlov.y@gmail.com>, 2008.
# Алексей Шилин <shilin.aleksej@gmail.com>, 2022.
#
msgid ""
msgstr ""
"Project-Id-Version: uif new\n"
"Report-Msgid-Bugs-To: uif@packages.debian.org\n"
"POT-Creation-Date: 2022-05-06 19:27+0200\n"
"PO-Revision-Date: 2022-05-18 21:27+0300\n"
"Last-Translator: Алексей Шилин <shilin.aleksej@gmail.com>\n"
"Language-Team: Russian <debian-l10n-russian@lists.debian.org>\n"
"Language: ru\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Gtranslator 3.38.0\n"
"Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n"
"%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2)\n"

#. Type: select
#. Choices
#: ../templates:1001
msgid "don't touch"
msgstr "не менять"

#. Type: select
#. Choices
#: ../templates:1001
msgid "workstation"
msgstr "рабочая станция"

#. Type: select
#. Choices
#: ../templates:1001
msgid "debian-edu-router"
msgstr "маршрутизатор Debian Edu"

#. Type: select
#. Description
#: ../templates:1002
msgid "Firewall configuration method"
msgstr "Тип настройки межсетевого экрана"

#. Type: select
#. Description
#: ../templates:1002
msgid ""
"Please choose whether the firewall should be configured now with a simple "
"\"workstation\" setup, given a specialized Debian Edu Router configuration, "
"or left unconfigured so that you can manually edit /etc/uif/uif.conf."
msgstr ""
"Выберите вариант настройки межсетевого экрана: простая конфигурация для "
"рабочей станции, специальная конфигурация для маршрутизатора Debian Edu, или "
"же оставить без изменений, чтобы вы могли вручную отредактировать /etc/uif/"
"uif.conf."

#. Type: string
#. Description
#: ../templates:2001
#| msgid "Enter trusted hosts and/or networks:"
msgid "Trusted DNS hostnames:"
msgstr "Доверенные доменные имена:"

#. Type: string
#. Description
#: ../templates:2001
#| msgid ""
#| "In workstation mode, you can specify some hosts or networks to be "
#| "globally trusted. All incoming traffic coming from there will be allowed. "
#| "Multiple entries have to be separate with spaces."
msgid ""
"In workstation mode, you can specify some DNS hostnames to be globally "
"trusted. All incoming traffic coming from these will be allowed. Multiple "
"entries must be separated with spaces."
msgstr ""
"В режиме «рабочая станция» вы можете указать несколько доменных имён узлов, "
"которым вы полностью доверяете. С них будет разрешён весь входящий трафик. "
"Несколько значений должны разделяться пробелами."

#. Type: string
#. Description
#: ../templates:2001
msgid ""
"Hostnames provided here must be resolvable to both IPv4 and IPv6 addresses."
msgstr ""
"Указанные здесь доменные имена должны разрешаться в как IPv4-, так и IPv6-"
"адреса."

#. Type: string
#. Description
#: ../templates:2001
msgid "Example: trusted-host-v4-and-v6.mydomain.com"
msgstr "Пример: trusted-host-v4-and-v6.mydomain.com"

#. Type: string
#. Description
#: ../templates:3001
#| msgid "Enter trusted hosts and/or networks:"
msgid "Trusted IPv4 hosts and/or networks:"
msgstr "Доверенные IPv4-узлы и/или сети:"

#. Type: string
#. Description
#: ../templates:3001
#| msgid ""
#| "In workstation mode, you can specify some hosts or networks to be "
#| "globally trusted. All incoming traffic coming from there will be allowed. "
#| "Multiple entries have to be separate with spaces."
msgid ""
"In workstation mode, you can specify some IPv4 hosts or networks to be "
"globally trusted. All incoming traffic coming from these will be allowed. "
"Multiple entries must be separated with spaces."
msgstr ""
"В режиме «рабочая станция» вы можете указать несколько IPv4-узлов или сетей, "
"которым вы полностью доверяете. С них будет разрешён весь входящий трафик. "
"Несколько значений должны разделяться пробелами."

#. Type: string
#. Description
#: ../templates:3001
msgid ""
"If you want to trust DNS hostnames that only resolve to an IPv4 address, "
"please enter them here."
msgstr ""
"Если существуют доверенные доменные имена узлов, которые разрешаются только "
"в IPv4-адреса, то введите их здесь."

#. Type: string
#. Description
#: ../templates:3001
#| msgid "Example: 10.1.0.0/16 trust.mydomain.com 192.168.1.55"
msgid "Example: 10.1.0.0/16 trusted-host-v4-only.mydomain.com 192.168.1.55"
msgstr "Пример: 10.1.0.0/16 trusted-host-v4-only.mydomain.com 192.168.1.55"

#. Type: string
#. Description
#: ../templates:4001
#| msgid "Enter trusted hosts and/or networks:"
msgid "Trusted IPv6 hosts and/or networks:"
msgstr "Доверенные IPv6-узлы и/или сети:"

#. Type: string
#. Description
#: ../templates:4001
#| msgid ""
#| "In workstation mode, you can specify some hosts or networks to be "
#| "globally trusted. All incoming traffic coming from there will be allowed. "
#| "Multiple entries have to be separate with spaces."
msgid ""
"In workstation mode, you can specify some IPv6 hosts or networks to be "
"globally trusted. All incoming traffic coming from these will be allowed. "
"Multiple entries must be separated with spaces."
msgstr ""
"В режиме «рабочая станция» вы можете указать несколько IPv6-узлов или сетей, "
"которым вы полностью доверяете. С них будет разрешён весь входящий трафик. "
"Несколько значений должны разделяться пробелами."

#. Type: string
#. Description
#: ../templates:4001
msgid ""
"If you want to trust DNS hostnames that only resolve with an IPv6 address, "
"please enter them here."
msgstr ""
"Если существуют доверенные доменные имена узлов, которые разрешаются только "
"в IPv6-адреса, то введите их здесь."

#. Type: string
#. Description
#: ../templates:4001
msgid "Example: 2001:1234:ab::1 fe80::1"
msgstr "Пример: 2001:1234:ab::1 fe80::1"

#. Type: boolean
#. Description
#: ../templates:5001
msgid "Allow ping?"
msgstr "Разрешить ping?"

#. Type: boolean
#. Description
#: ../templates:5001
#| msgid ""
#| "Normally an Internet host should be reachable with pings. Choosing no "
#| "here will disable pings which might be somewhat confusing when analyzing "
#| "network problems."
msgid ""
"Normally an Internet host should be reachable with \"pings\" (ICMP Echo "
"requests). Rejecting this option will disable this, which might be somewhat "
"confusing when analyzing network problems."
msgstr ""
"Обычно узлы в Интернете должны быть доступны для «ping» (запросов ICMP "
"Echo). При отрицательном ответе реакция узла на «ping» будет заблокирована, "
"что может затруднить анализ в случае проблем с сетью."

#. Type: boolean
#. Description
#: ../templates:6001
msgid "Allow traceroute?"
msgstr "Разрешить traceroute?"

#. Type: boolean
#. Description
#: ../templates:6001
#| msgid ""
#| "Normally an Internet host should react to traceroutes. Choosing no here "
#| "will disable this, which might be somewhat confusing when analyzing "
#| "network problems."
msgid ""
"Normally an Internet host should react to traceroute test packets. Rejecting "
"this option will disable this, which might be somewhat confusing when "
"analyzing network problems."
msgstr ""
"Обычно узлы в Интернете должны реагировать на тестовые пакеты traceroute. "
"При отрицательном ответе реакция узла на пакеты traceroute будет "
"заблокирована, что может затруднить анализ в случае проблем с сетью."

#. Type: boolean
#. Description
#: ../templates:7001
#| msgid "Firewall for simple workstation setups"
msgid "Really set up up a simple workstation firewall?"
msgstr "Настроить простой межсетевой экран для рабочих станций?"

#. Type: boolean
#. Description
#: ../templates:7001
#| msgid ""
#| "Warning: This configuration provides a very simple firewall setup which "
#| "is only able to trust certain hosts and configure global ping / "
#| "traceroute behaviour."
msgid ""
"Warning: This configuration only provides a very simple firewall setup, "
"specifying certain hosts as trusted and configuring responses to ping and "
"traceroute."
msgstr ""
"Предупреждение: здесь выполняется лишь очень простая настройка межсетевого "
"экрана, при которой задаются доверенные узлы и реакция на запросы ping и "
"traceroute."

#. Type: boolean
#. Description
#: ../templates:7001
#| msgid ""
#| "If you need a more specific setup, use /etc/uif/uif.conf as a template "
#| "and choose \"don't touch\" next time."
msgid ""
"If you need a more complex setup, use /etc/uif/uif.conf as a template and "
"choose \"don't touch\" next time."
msgstr ""
"Если вам требуется более сложная настройка, то используйте /etc/uif/uif.conf "
"в качестве шаблона, и в следующий раз выберите «не менять»."

#. Type: boolean
#. Description
#: ../templates:8001
#| msgid "Firewall for simple workstation setups"
msgid "Really set up the firewall for Debian Edu Router?"
msgstr "Настроить межсетевой экран для маршрутизатора Debian Edu?"

#. Type: boolean
#. Description
#: ../templates:8001
msgid ""
"Warning: This configuration provides a base setup for the Debian Edu Router, "
"which basically blocks all incoming/outgoing traffic."
msgstr ""
"Предупреждение: эта конфигурация предоставляет базовые настройки для "
"маршрутизатора Debian Edu, блокирующие весь входящий и исходящий трафик."

#. Type: boolean
#. Description
#: ../templates:8001
msgid "Don't use this setup unless you know what you are doing."
msgstr "Не используйте эти настройки, если вы не знаете, что делаете."

#. Type: error
#. Description
#: ../templates:9001
msgid "Error in list of trusted hosts"
msgstr "Ошибка в списке доверенных узлов"

#. Type: error
#. Description
#: ../templates:9001
#| msgid ""
#| "Please check the hosts / networks you entered. One or more entries are "
#| "not correct, contain no resolvable hosts, valid IP-addresses, valid "
#| "network definitions or masks."
msgid ""
"One or more hosts or networks entered have errors. Please ensure that hosts "
"are resolvable, and that IP addresses, network definitions, and masks are "
"valid."
msgstr ""
"Один или несколько введённых узлов или сетей содержат ошибки. Убедитесь, что "
"для узлов работает разрешение имён, и что указаны правильные IP-адреса, сети "
"и маски."

#~ msgid ""
#~ "The firewall can be initialized using debconf, or using information you "
#~ "manually put into /etc/uif/uif.conf."
#~ msgstr ""
#~ "Настройка межсетевого экрана может быть выполнена с помощью debconf или "
#~ "ручного редактирования файла /etc/uif/uif.conf."

#~ msgid "Do you want your host to be reachable via ping?"
#~ msgstr "Разрешить ответы хоста на ping?"

#~ msgid "Do you want your host to react to traceroutes?"
#~ msgstr "Разрешить ответы хоста на traceroute?"

#~ msgid "don't touch, workstation"
#~ msgstr "не менять, рабочая станция"
