Template: uif/conf_method
Type: select
__Choices: don't touch, workstation, debian-edu-router
Default: don't touch
_Description: Firewall configuration method
 Please choose whether the firewall should be configured now with a
 simple "workstation" setup, given a specialized Debian Edu Router
 configuration, or left unconfigured so that you can manually edit
 /etc/uif/uif.conf.

Template: uif/trusted-hostnames
Type: string
_Description: Trusted DNS hostnames:
 In workstation mode, you can specify some DNS hostnames to be
 globally trusted. All incoming traffic coming from these will be
 allowed. Multiple entries must be separated with spaces.
 .
 Hostnames provided here must be resolvable to both IPv4 and IPv6
 addresses.
 .
 Example: trusted-host-v4-and-v6.mydomain.com

Template: uif/trusted
Type: string
_Description: Trusted IPv4 hosts and/or networks:
 In workstation mode, you can specify some IPv4 hosts or networks to be
 globally trusted. All incoming traffic coming from these will be
 allowed. Multiple entries must be separated with spaces.
 .
 If you want to trust DNS hostnames that only resolve to
 an IPv4 address, please enter them here.
 .
 Example: 10.1.0.0/16 trusted-host-v4-only.mydomain.com 192.168.1.55

Template: uif/trusted-v6
Type: string
_Description: Trusted IPv6 hosts and/or networks:
 In workstation mode, you can specify some IPv6 hosts or networks to be
 globally trusted. All incoming traffic coming from these will be
 allowed. Multiple entries must be separated with spaces.
 .
 If you want to trust DNS hostnames that only resolve with
 an IPv6 address, please enter them here.
 .
 Example: 2001:1234:ab::1 fe80::1

Template: uif/pings
Type: boolean
Default: true
_Description: Allow ping?
 Normally an Internet host should be reachable with "pings" (ICMP Echo
 requests). Rejecting this option will disable this, which might be
 somewhat confusing when analyzing network problems.

Template: uif/traceroute
Type: boolean
Default: true
_Description: Allow traceroute?
 Normally an Internet host should react to traceroute test packets.
 Rejecting this option will disable this, which might be somewhat
 confusing when analyzing network problems.

Template: uif/really-setup-workstation
Type: boolean
_Description: Really set up up a simple workstation firewall?
 Warning: This configuration only provides a very simple firewall setup,
 specifying certain hosts as trusted and configuring responses to ping
 and traceroute.
 .
 If you need a more complex setup, use /etc/uif/uif.conf as a template and
 choose "don't touch" next time.

Template: uif/really-setup-debianedurouter
Type: boolean
_Description: Really set up the firewall for Debian Edu Router?
 Warning: This configuration provides a base setup for the Debian Edu
 Router, which basically blocks all incoming/outgoing traffic.
 .
 Don't use this setup unless you know what you are doing.

Template: uif/error
Type: error
_Description: Error in list of trusted hosts
 One or more hosts or networks entered have errors. Please ensure that
 hosts are resolvable, and that IP addresses, network definitions, and
 masks are valid.
